from django.urls import path

from . import views

app_name = 'trigger3'

urlpatterns = [
    path('', views.index, name='index'),
    path('read-psd', views.read_psd, name='read_psd'),
    path('read-psd-adminsatgas', views.read_psd_adminsatgas, name='read_psd_adminsatgas'),
    path('read-psd-supplier', views.read_psd_supplier, name='read_psd_supplier'),
    path('psd', views.psd, name='psd'),
    path('create-psd', views.create_psd, name='create_psd'),
    path('tambah-psd', views.tambah_psd, name='tambah_psd'),
    path('tampil-update-psd/<int:no_pesanan>', views.tampil_update_psd, name='tampil_update_psd'),
    path('update-psd/<int:no_pesanan>', views.update_psd, name='update_psd'),
    path('delete-psd/<int:no_pesanan>', views.delete_psd, name='delete_psd'),
    path('rspsd', views.rspsd, name='rspsd'),
    path('read-rspsd/<int:no_pesanan>', views.read_rspsd, name='read_rspsd'),
    path('create-status/<int:no_pesanan>', views.create_status, name='create_status'),
    path('sfk-adminsatgas', views.sfk_adminsatgas, name='sfk_adminsatgas'),
    path('sfk-petugasfaskes', views.sfk_petugas, name='sfk_petugas'),
    path('sfk', views.sfk, name='sfk'),
    path('create-sfk', views.create_sfk, name='create_sfk'),
    path('tampil-update-sfk/<str:kode_faskes>/<str:item_sumber_daya>', views.tampil_update_sfk, name='tampil_update_sfk'),
    path('update-sfk', views.update_sfk, name='update_sfk'),
    path('delete-sfk/<str:kode_faskes>/<str:item_sumber_daya>', views.delete_sfk, name='delete_sfk'),
    path('detail-psd/<int:no_pesanan>', views.detail_psd, name='detail_psd')
]