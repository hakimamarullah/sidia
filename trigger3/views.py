from django.shortcuts import render, redirect
from django.db import connection, IntegrityError
from django.db.utils import InternalError
from django.contrib import messages
from datetime import datetime, date
import random


def index(request):
    context = {
        "role" : request.session["role"]
    }

    return render(request, 'trigger3/index.html', context)

def rspsd(request):
    response = {}
    response['rsp'] = []
    with connection.cursor() as cursor:
        cursor.execute(f"""SELECT DISTINCT ON (no_pesanan) kode_status_pesanan, no_pesanan, username_supplier, tanggal 
                       FROM RIWAYAT_STATUS_PESANAN""")
        rsp = cursor.fetchall()
        for i in range(len(rsp)):
            response['rsp'].append([
                rsp[i][0],
                rsp[i][1],
                rsp[i][2],
                rsp[i][3],
            ])
    return render(request, 'trigger3/rspsd.html', response)
    
def read_rspsd(request, no_pesanan):
    response = {}
    response['rsp'] = []
    with connection.cursor() as cursor:
        cursor.execute(f"""SELECT * FROM HISTORY_STATUS_PESANAN WHERE no_pesanan={no_pesanan}
                       ORDER BY tanggal DESC""")
        rsp = cursor.fetchall()
        for i in range(len(rsp)):
            response['rsp'].append([
                rsp[i][0],
                rsp[i][1],
                rsp[i][2],
                rsp[i][3],
            ])
    return render(request, 'trigger3/rsp.html', response)

def create_status(request, no_pesanan):
    response = {}
    if request.POST.get('process', False) == "REQ-SUP":
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""UPDATE RIWAYAT_STATUS_PESANAN SET kode_status_pesanan='PRO-SUP', no_pesanan={no_pesanan},
                               username_supplier='{request.session['pengguna']}', tanggal='{date.today()}'
                               WHERE no_pesanan={no_pesanan} AND kode_status_pesanan='REQ-SUP'""")
                
                cursor.execute(f"""INSERT INTO HISTORY_STATUS_PESANAN (kode_status_pesanan, no_pesanan, username_supplier, tanggal)
                            VALUES ('PRO-SUP', {no_pesanan}, '{request.session['pengguna']}', '{datetime.now()}')""")
                
                messages.add_message(request, messages.SUCCESS, "Berhasil menambah riwayat status pesanan")
            
            return redirect('trigger3:read_rspsd', no_pesanan=no_pesanan)
        except InternalError:
            messages.add_message(request, messages.WARNING, "Database sudah penuh")
        except:
            messages.add_message(request, messages.WARNING, "Ada gangguan, mohon mencoba lain kali")
    elif request.POST.get('reject', False) == "REQ-SUP":
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""UPDATE RIWAYAT_STATUS_PESANAN SET kode_status_pesanan='REJ-SUP', no_pesanan={no_pesanan},
                               username_supplier='{request.session['pengguna']}', tanggal='{date.today()}'
                               WHERE no_pesanan={no_pesanan} AND kode_status_pesanan='REQ-SUP'""")
                
                cursor.execute(f"""INSERT INTO HISTORY_STATUS_PESANAN (kode_status_pesanan, no_pesanan, username_supplier, tanggal)
                            VALUES ('REJ-SUP', {no_pesanan}, '{request.session['pengguna']}', '{datetime.now()}')""")
                
                messages.add_message(request, messages.SUCCESS, "Berhasil menambah riwayat status pesanan")
                return redirect('trigger3:read_rspsd', no_pesanan=no_pesanan)
        except InternalError:
            messages.add_message(request, messages.WARNING, "Database sudah penuh")
        except:
            messages.add_message(request, messages.WARNING, "Ada gangguan, mohon mencoba lain kali")
    elif request.POST.get('selesai-dm', False) == "PRO-SUP":
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""UPDATE RIWAYAT_STATUS_PESANAN SET kode_status_pesanan='MAS-SUP', no_pesanan={no_pesanan},
                               username_supplier='{request.session['pengguna']}', tanggal='{date.today()}'
                               WHERE no_pesanan={no_pesanan} AND kode_status_pesanan='PRO-SUP'""")
                
                cursor.execute(f"""INSERT INTO HISTORY_STATUS_PESANAN (kode_status_pesanan, no_pesanan, username_supplier, tanggal)
                            VALUES ('MAS-SUP', {no_pesanan}, '{request.session['pengguna']}', '{datetime.now()}')""")
                
                messages.add_message(request, messages.SUCCESS, "Berhasil menambah riwayat status pesanan")
                return redirect('trigger3:read_rspsd', no_pesanan=no_pesanan)
        except InternalError:
            messages.add_message(request, messages.WARNING, "Database sudah penuh")
        except:
            messages.add_message(request, messages.WARNING, "Ada gangguan, mohon mencoba lain kali")
    elif request.POST.get('selesai', False) == "PRO-SUP":
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""UPDATE RIWAYAT_STATUS_PESANAN SET kode_status_pesanan='FIN-SUP', no_pesanan={no_pesanan},
                               username_supplier='{request.session['pengguna']}', tanggal='{date.today()}'
                               WHERE no_pesanan={no_pesanan} AND kode_status_pesanan='PRO-SUP'""")

                cursor.execute(f"""INSERT INTO HISTORY_STATUS_PESANAN (kode_status_pesanan, no_pesanan, username_supplier, tanggal)
                            VALUES ('FIN-SUP', {no_pesanan}, '{request.session['pengguna']}', '{datetime.now()}')""")
                
                messages.add_message(request, messages.SUCCESS, "Berhasil menambah riwayat status pesanan")
                return redirect('trigger3:read_rspsd', no_pesanan=no_pesanan)
        except InternalError:
            messages.add_message(request, messages.WARNING, "Database sudah penuh")
        except:
            messages.add_message(request, messages.WARNING, "Ada gangguan, mohon mencoba lain kali")
            
    return render(request, 'trigger3/rsp.html', response)

def read_psd(request):
    response = {}
    response['psd'] = []
    with connection.cursor() as cursor:
        cursor.execute(f"""SELECT DISTINCT ON (no_pesanan) no_pesanan, username_admin_satgas, 
                       t.tanggal, total_berat, total_item, total_harga, kode_status_pesanan 
                       FROM TRANSAKSI_SUMBER_DAYA t, PESANAN_SUMBER_DAYA p, RIWAYAT_STATUS_PESANAN r;""")
        psd = cursor.fetchall()
        for i in range(len(psd)):
            response['psd'].append([
                psd[i][0],
                psd[i][1],
                psd[i][2],
                psd[i][3],
                psd[i][4],
                psd[i][5],
                psd[i][6],
            ])
    return render(request, 'trigger3/read-psd.html', response)

def read_psd_adminsatgas(request):
    response = {}
    response['psd'] = []
    with connection.cursor() as cursor:
        cursor.execute(f"""
                       SELECT DISTINCT ON (no_pesanan) no_pesanan, username_admin_satgas, 
                       t.tanggal, total_berat, total_item, total_harga, kode_status_pesanan, username_supplier 
                       FROM TRANSAKSI_SUMBER_DAYA t, PESANAN_SUMBER_DAYA p, RIWAYAT_STATUS_PESANAN r
                       WHERE username_admin_satgas='{request.session['pengguna']}'""")
        psd = cursor.fetchall()
        for i in range(len(psd)):
            response['psd'].append([
                psd[i][0],
                psd[i][1],
                psd[i][2],
                psd[i][3],
                psd[i][4],
                psd[i][5],
                psd[i][6],
                psd[i][7],
            ])
    return render(request, 'trigger3/read-psd.html', response)

def read_psd_supplier(request):
    response = {}
    response['psd'] = []
    with connection.cursor() as cursor:
        cursor.execute(f"""
                       SELECT DISTINCT ON (no_pesanan) no_pesanan, username_admin_satgas, 
                       t.tanggal, total_berat, total_item, total_harga, kode_status_pesanan, username_supplier 
                       FROM TRANSAKSI_SUMBER_DAYA t, PESANAN_SUMBER_DAYA p, RIWAYAT_STATUS_PESANAN r
                       WHERE username_supplier='{request.session['pengguna']}'""")
        psd = cursor.fetchall()
        for i in range(len(psd)):
            response['psd'].append([
                psd[i][0],
                psd[i][1],
                psd[i][2],
                psd[i][3],
                psd[i][4],
                psd[i][5],
                psd[i][6],
                psd[i][7],
            ])
    return render(request, 'trigger3/read-psd.html', response)

def psd(request):
    response = {}
    with connection.cursor() as cursor:
        cursor.execute(f"""SELECT username FROM SUPPLIER""")
        suppliers = cursor.fetchall()
        for i in range(len(suppliers)):
            suppliers[i] = suppliers[i][0]
        response['suppliers'] = suppliers
    return render(request, 'trigger3/psd.html', response)

def create_psd(request):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    SELECT kode FROM ITEM_SUMBER_DAYA
                    WHERE username_supplier = '{request.POST["supplier"]}'""")

                kodes = cursor.fetchall()
                idnom = 1
                
                for i in range(len(kodes)):
                    kodes[i] = kodes[i][0]
                    
                response['kodes'] = kodes
                response['no_urut'] = idnom
                response['supplier'] = request.POST["supplier"]

                # return render(request, 'trigger3/create-psd.html', response)
                messages.add_message(request, messages.SUCCESS, f"Supplier terpilih")
        
            except InternalError:
                messages.add_message(request, messages.WARNING, f"Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger3/create-psd.html', response)

def tambah_psd(request):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""SELECT * FROM TRANSAKSI_SUMBER_DAYA""")
                tsd_rows = len(cursor.fetchall()) + 1
                cursor.execute(f"""
                    INSERT INTO TRANSAKSI_SUMBER_DAYA (nomor, tanggal) VALUES ({tsd_rows}, '{date.today()}');
                    INSERT INTO PESANAN_SUMBER_DAYA (nomor_pesanan, username_admin_satgas)
                        VALUES ({tsd_rows}, '{request.session['pengguna']}');
                    INSERT INTO RIWAYAT_STATUS_PESANAN (kode_status_pesanan, no_pesanan, username_supplier, tanggal)
                        VALUES('REQ-SUP', {tsd_rows}, '{request.POST['supplier']}', '{date.today()}');
                    INSERT INTO HISTORY_STATUS_PESANAN (kode_status_pesanan, no_pesanan, username_supplier, tanggal)
                        VALUES ('REQ-SUP', {tsd_rows}, '{request.POST['supplier']}', '{datetime.now()}');
                    INSERT INTO DAFTAR_ITEM (no_transaksi_sumber_daya, no_urut, jumlah_item, kode_item_sumber_daya)
                        VALUES ({tsd_rows}, {request.POST['no_urut']}, {request.POST['jumlah_item']}, '{request.POST['kode_item']}');
                    """)
                
                cursor.execute(f"""UPDATE PESANAN_SUMBER_DAYA SET total_harga=(select (harga_satuan*total_item) 
                        FROM item_sumber_daya, transaksi_sumber_daya WHERE kode='{request.POST['kode_item']}' and nomor={tsd_rows}) WHERE nomor_pesanan={tsd_rows};""")
                
                return redirect('trigger3:index')
        
            except InternalError:
                messages.add_message(request, messages.WARNING, f"Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger3/create-psd.html', response)

def tampil_update_psd(request, no_pesanan):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    SELECT kode FROM ITEM_SUMBER_DAYA
                    WHERE username_supplier = '{request.POST["supplier"]}'""")

                kodes = cursor.fetchall()
                idnom = 1
                
                for i in range(len(kodes)):
                    kodes[i] = kodes[i][0]
                    
                response['kodes'] = kodes
                response['no_urut'] = idnom
                response['supplier'] = request.POST["supplier"]
                response['no_pesanan'] = no_pesanan
        
            except InternalError:
                messages.add_message(request, messages.WARNING, f"Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger3/update-psd.html', response)

def update_psd(request, no_pesanan):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""UPDATE TRANSAKSI_SUMBER_DAYA SET tanggal='{date.today()}' WHERE nomor={no_pesanan}""")
                
                cursor.execute(f"""UPDATE DAFTAR_ITEM SET kode_item_sumber_daya='{request.POST['kode_item']}',
                               jumlah_item={request.POST['jumlah_item']}
                               WHERE no_transaksi_sumber_daya={no_pesanan}""")
                
                messages.add_message(request, messages.SUCCESS, "Data berhasil diupdate")
                return redirect('trigger3:read_psd_adminsatgas')
            except InternalError:
                messages.add_message(request, messages.WARNING, f"Database sudah penuh")
            # except:
            #     messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger3/read-psd.html', response)

def delete_psd(request, no_pesanan):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    DELETE FROM PESANAN_SUMBER_DAYA
                    WHERE nomor_pesanan = {request.POST["no_pesanan"]}""")
                
                messages.add_message(request, messages.SUCCESS, "Data berhasil dihapus")
                return redirect('trigger3:read_psd_adminsatgas')
            except InternalError:
                messages.add_message(request, messages.WARNING, "Database sudah penuh")
            # except:
            #     messages.add_message(request, messages.WARNING, "Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger3/read-psd.html', response)

def detail_psd(request, no_pesanan):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    SELECT DISTINCT ON (no_pesanan) no_pesanan, username_admin_satgas, 
                    t.tanggal, total_berat, total_item, total_harga, kode_status_pesanan, username_supplier 
                    FROM TRANSAKSI_SUMBER_DAYA t, PESANAN_SUMBER_DAYA p, RIWAYAT_STATUS_PESANAN r
                    WHERE no_pesanan={no_pesanan}""")

                details = cursor.fetchall()
                
                response['no_pesanan'] = details[1][0]
                response['username_admin_satgas'] = details[0][1]
                response['tanggal'] = details[0][2]
                response['total_berat'] = details[0][3]
                response['total_item'] = details[0][4]
                response['total_harga'] = details[0][5]
                response['kode_status_pesanan'] = details[0][6]
                
                response['details'] = details
                
                cursor.execute(f"""SELECT t.nama, i.harga_satuan, d.jumlah_item FROM DAFTAR_ITEM d, ITEM_SUMBER_DAYA i, TIPE_ITEM t
                               WHERE d.no_transaksi_sumber_daya={no_pesanan} AND d.kode_item_sumber_daya=i.kode AND t.kode=i.kode_tipe_item""")
                
                items = cursor.fetchall()
                for i in range(len(items)):
                    response['items'].append([
                        items[i][0],
                        items[i][1],
                        items[i][2],
                    ])
        
            except InternalError:
                messages.add_message(request, messages.WARNING, f"Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger3/detail-psd.html', response)

def sfk_adminsatgas(request):
    response = {}
    response['sfk'] = []
    with connection.cursor() as cursor:
        cursor.execute(f"""SELECT s.kode_faskes, t.nama_tipe, i.kode as item_sumber_daya, s.jumlah 
                       FROM STOK_FASKES s, TIPE_FASKES t, FASKES f, ITEM_SUMBER_DAYA i
                       WHERE s.kode_faskes=f.kode_faskes_nasional AND s.kode_item_sumber_daya=i.kode AND f.kode_tipe_faskes=t.kode;""")
        sfk = cursor.fetchall()
        for i in range(len(sfk)):
            response['sfk'].append([
                sfk[i][0],
                sfk[i][1],
                sfk[i][2],
                sfk[i][3],
            ])
    return render(request, 'trigger3/read-sfk.html', response)

def sfk_petugas(request):
    response = {}
    response['sfk'] = []
    with connection.cursor() as cursor:
        cursor.execute(f"""SELECT s.kode_faskes, t.nama_tipe, i.kode as item_sumber_daya, s.jumlah 
                       FROM STOK_FASKES s, TIPE_FASKES t, FASKES f, ITEM_SUMBER_DAYA i
                       WHERE s.kode_faskes=f.kode_faskes_nasional AND s.kode_item_sumber_daya=i.kode AND f.kode_tipe_faskes=t.kode
                       AND f.username_petugas='{request.session['pengguna']}';""")
        sfk = cursor.fetchall()
        for i in range(len(sfk)):
            response['sfk'].append([
                sfk[i][0],
                sfk[i][1],
                sfk[i][2],
                sfk[i][3],
            ])
    return render(request, 'trigger3/read-sfk.html', response)

def sfk(request):
    response = {}
    with connection.cursor() as cursor:
        cursor.execute(f"""SELECT kode_faskes_nasional FROM FASKES""")
        kodes = cursor.fetchall()
        for i in range(len(kodes)):
            kodes[i] = kodes[i][0]
        response['kodes'] = kodes
        
        cursor.execute(f"""SELECT kode FROM ITEM_SUMBER_DAYA""")
        item_codes = cursor.fetchall()
        for i in range(len(item_codes)):
            item_codes[i] = item_codes[i][0]
        response['item_codes'] = item_codes
        
    return render(request, 'trigger3/create-sfk.html', response)

def create_sfk(request):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    INSERT INTO STOK_FASKES (kode_faskes, kode_item_sumber_daya, jumlah) 
                        VALUES ('{request.POST["kode_faskes"]}', '{request.POST["kode_item"]}', '{request.POST["jumlah"]}');""")

                return redirect('trigger3:index')
        
            except InternalError:
                messages.add_message(request, messages.WARNING, f"Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger3/create-psd.html', response)

def tampil_update_sfk(request, **kwargs):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                response['kode_faskes'], response['kode_item'] = kwargs.get('kode_faskes'), kwargs.get('item_sumber_daya');
                
                cursor.execute(f"""SELECT nama_tipe FROM FASKES f, TIPE_FASKES t
                               WHERE kode_faskes_nasional='{kwargs.get('kode_faskes')}' AND f.kode_tipe_faskes=t.kode""")
                
                faskes = cursor.fetchall()
                response['faskes'] = faskes[0][0]
                
                cursor.execute(f"""SELECT t.nama FROM ITEM_SUMBER_DAYA i, TIPE_ITEM t
                               WHERE i.kode='{kwargs.get('item_sumber_daya')}' AND i.kode_tipe_item=t.kode""")
                
                item_sd = cursor.fetchall()
                response['item_sumber_daya'] = item_sd[0][0]
        
            except InternalError:
                messages.add_message(request, messages.WARNING, f"Database sudah penuh")
            # except:
            #     messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger3/update-sfk.html', response)

def update_sfk(request):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""UPDATE STOK_FASKES SET jumlah={request.POST["jumlah"]} 
                               WHERE kode_faskes='{request.POST["kode_faskes"]}' AND kode_item_sumber_daya='{request.POST["kode_item"]}'""")
                
                messages.add_message(request, messages.SUCCESS, "Data berhasil diupdate")
                return redirect('trigger3:sfk_adminsatgas')
            except InternalError:
                messages.add_message(request, messages.WARNING, f"Database sudah penuh")
            # except:
            #     messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger3/read-sfk.html', response)

def delete_sfk(request, **kwargs):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    DELETE FROM STOK_FASKES
                    WHERE kode_faskes='{kwargs.get('kode_faskes')}' AND kode_item_sumber_daya='{kwargs.get('item_sumber_daya')}'""")
                
                messages.add_message(request, messages.SUCCESS, "Data berhasil dihapus")
                return redirect('trigger3:sfk_adminsatgas')
            except InternalError:
                messages.add_message(request, messages.WARNING, "Database sudah penuh")
            # except:
            #     messages.add_message(request, messages.WARNING, "Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger3/read-sfk.html', response)