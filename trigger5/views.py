from django.shortcuts import render, redirect
from django.db import connection, IntegrityError
from django.db.utils import InternalError
from django.contrib import messages
from datetime import datetime
import random

# Create your views here.
def index(request):
    context = {
        "role" : request.session["role"]
    }

    return render(request, 'trigger5/index.html', context)
    
def faskes(request):
    response = {}
    response['faskes'] = []
    with connection.cursor() as cursor:
        cursor.execute('''
        SELECT DISTINCT f.kode_faskes_nasional, l.jalan_no, l.kelurahan, l.kecamatan, l.kabkot, l.provinsi, f.username_petugas,  t.nama_tipe
        FROM FASKES f, LOKASI l, TIPE_FASKES t  
        WHERE f.kode_tipe_faskes = t.kode
        AND f.id_lokasi = l.id''')
        fetched = cursor.fetchall()
        for i in range(len(fetched)):
            response['faskes'].append([
                fetched[i][0],
                fetched[i][1] +", "+ fetched[i][2]+", "+fetched[i][3]+", "+fetched[i][4]+", "+fetched[i][5],
                fetched[i][6],
                fetched[i][7],
            ]) 
    return render(request, 'trigger5/faskes.html', response)
 
def create_faskes(request):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                codelen = len(str(request.POST["kode"])) - 2
                userlen = len(str(request.POST["petugas"])) - 2
                location = str({request.POST["id_loc"]})[2:7]
                codex = str(request.POST["kode"])
                userss = str(request.POST["petugas"])

                cursor.execute(f"""
                    INSERT INTO FASKES (kode_faskes_nasional, id_lokasi, username_petugas, kode_tipe_faskes) VALUES
                    ('{request.POST["codex"]}', '{location}', '{userss}', '{codex}') """)
                # cursor.execute(f"""
                #     INSERT INTO FASKES (kode_faskes_nasional, id_lokasi, username_petugas, kode_tipe_faskes) VALUES
                #     ('{request.POST["kode_faskes_nasional"]}', '{request.POST["id_loc"]}', '{request.POST["petugas"]}', '{request.POST["kode"]}') """)
                
                messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil")
                return redirect("trigger5:faskes")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            # except:
            #     messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")

    else:
        response['id_lokasi'] = []
        response['petugas_faskes'] = []
        response['kode_tipe_faskes'] = []
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM LOKASI")
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['id_lokasi'].append(fetched[i][0])
                
            cursor.execute("SELECT * FROM petugas_faskes")
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['petugas_faskes'].append(fetched[i][0]) 
                
            cursor.execute("SELECT * FROM tipe_faskes")
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['kode_tipe_faskes'].append(fetched[i][0])

    return render(request, 'trigger5/create_faskes.html', response)
 
def update_faskes(request):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                lenloc = len(str(request.POST["id_loc"])) - 2
                userloc = len(str(request.POST["petugas"])) - 2
                codeloc = len(str(request.POST["kode_faskes_nasional"])) - 2
                codnas = str(request.POST["kode_faskes_nasional"])
                usernames = str(request.POST["petugas"])
                codex = str(request.POST["kode"])

                cursor.execute(f""" 
                    UPDATE FASKES SET 
                        kode_faskes_nasional='{codnas}',
                        id_lokasi='{idloc}',
                        username_petugas='{usernames}',
                        kode_tipe_faskes = '{codex}'
                        WHERE kode_faskes_nasional = '{codnas}'
                    """)
                messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil")
                return redirect("trigger5:faskes")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")

    else:
        response["kode_faskes_nasional"] = []
        response['id_lokasi'] = []
        response['petugas_faskes'] = []
        response['kode_tipe_faskes'] = []
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM faskes")
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['kode_faskes_nasional'].append(fetched[i][0]) 

            cursor.execute("SELECT * FROM LOKASI")
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['id_lokasi'].append(fetched[i][0])

            cursor.execute("SELECT * FROM petugas_faskes")
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['petugas_faskes'].append(fetched[i][0]) 
                
            cursor.execute("SELECT * FROM tipe_faskes")
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['kode_tipe_faskes'].append(fetched[i][0])
    return render(request, 'trigger5/update_faskes.html', response)

def delete_faskes(request):
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""DELETE FROM FASKES WHERE kode_faskes_nasional='{request.POST["kode_faskes_nasional"]}'""")
                messages.add_message(request, messages.SUCCESS, f"Penghapusan berhasil")
                return redirect("trigger5:faskes")

            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger5/delete_faskes.html')

def warehouse(request):
    response = {}
    response['lokasi'] = []
    with connection.cursor() as cursor:
        cursor.execute('''
        SELECT DISTINCT l.jalan_no, l.kelurahan, l.kecamatan, l.kabkot, l.provinsi
        FROM LOKASI l, WAREHOUSE_PROVINSI w
        WHERE l.id in (SELECT id_lokasi FROM WAREHOUSE_PROVINSI)
        ''')
        
        fetched = cursor.fetchall()
        for i in range(len(fetched)):
            response['lokasi'].append([
                fetched[i][4],
                str(fetched[i][0]).capitalize()+", "+ str(fetched[i][1]).capitalize()+", "+str(fetched[i][2]).capitalize()+", "+str(fetched[i][3]).capitalize()
            ]) 

    return render(request, 'trigger5/warehouse.html', response)
 
def create_warehouse(request):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                location = str(request.POST["id_lokasi"])
                cursor.execute(f"""SELECT l.provinsi FROM LOKASI l, WAREHOUSE_PROVINSI wp WHERE l.id=wp.id_lokasi AND wp.id_lokasi='{location}' AND l.provinsi='{request.POST["provinsi"]}'""")
                fetched = cursor.fetchall()
                if len(fetched) == 0:
                    cursor.execute(f"""
                        INSERT INTO WAREHOUSE_PROVINSI (id_lokasi) VALUES ('{location}') """)
        
                messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil")
                return redirect("trigger5:warehouse")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")

    else:
        response['lokasi'] = []
        response['provinsi'] = []
        with connection.cursor() as cursor:
            cursor.execute('''
            SELECT DISTINCT *
            FROM LOKASI 
            ''')
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['lokasi'].append(fetched[i][0]) 
                # response['provinsi'].append([
                #     fetched[i][1],
                # ]) 

    return render(request, 'trigger5/create_warehouse.html', response)
 
def update_warehouse(request):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                locawal = str(request.POST["id_lokasi_awal"])#[2:7]
                locganti = str(request.POST["id_lokasi_ganti"])#[2:7]
                cursor.execute(f""" 
                    UPDATE WAREHOUSE_PROVINSI SET 
                        id_lokasi='{locganti}'
                        WHERE id_lokasi = '{locawal}'
                    """)
                messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil")
                return redirect("trigger5:warehouse")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")

    else:
        response['lokasi'] = []
        response['lokasi2'] = []
        with connection.cursor() as cursor:
            # cursor.execute('''
            # SELECT DISTINCT *
            # FROM LOKASI l, WAREHOUSE_PROVINSI w
            # WHERE l.id NOT IN (SELECT id_lokasi FROM WAREHOUSE_PROVINSI)
            # ''')
            # fetched = cursor.fetchall()
            # for i in range(len(fetched)):
            #     response['lokasi'].append([
            #         fetched[i][0],
            #     ]) 
            #     response['provinsi'].append([
            #         fetched[i][1],
            #     ])
            cursor.execute('''
            SELECT * FROM WAREHOUSE_PROVINSI ''')
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['lokasi'].append(fetched[i][0]) 
            cursor.execute('''
            SELECT * FROM LOKASI ''')
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['lokasi2'].append(fetched[i][0]) 
    return render(request, 'trigger5/update_warehouse.html', response)
 
def delete_warehouse(request):
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""DELETE FROM WAREHOUSE_PROVINSI WHERE id_lokasi='{request.POST["id_lokasi"]}'""")
                messages.add_message(request, messages.SUCCESS, f"Penghapusan berhasil")
                return redirect("trigger5:warehouse")
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")

    return render(request, 'trigger5/delete_warehouse.html')

def batch(request):
    context = {
        "role" : request.session["role"]
    }

    return render(request, 'trigger5/batch.html', context)

def batch_lokasi(request):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                length = len(str(request.POST["cars"])) - 2
                coded1 = str(request.POST["asal"])#[2:7]
                coded2 = str(request.POST["tujuan"])#[2:7]
                coded3 = str(request.POST["cars"])#[2:length]
                coded4 = str(request.POST["batch_code"])#[2:5]

                cursor.execute(f""" UPDATE BATCH_PENGIRIMAN SET id_lokasi_asal='{coded1}', id_lokasi_tujuan='{coded2}', no_kendaraan='{coded3}' WHERE kode='{coded4}' """)
                
                messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil")
                return redirect("trigger5:batch")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")

    else:
        response['code'] = []
        response['kodex'] = []
        response['vehicle'] = []
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM LOKASI")
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['code'].append(fetched[i][0]) 
            cursor.execute("SELECT * FROM KENDARAAN")
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['vehicle'].append(fetched[i][0]) 

            cursor.execute("SELECT * FROM BATCH_PENGIRIMAN")
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['kodex'].append(fetched[i][0]) 
    return render(request, 'trigger5/batch_lokasi.html', response)

def detail_batch(request):
    response = {}
    response['batches'] = []
    with connection.cursor() as cursor:
        cursor.execute(f""" SELECT * FROM BATCH_PENGIRIMAN """)
        loc = cursor.fetchall()
        for i in range(len(loc)):

            response['batches'].append([
                loc[i][0],
                loc[i][1],
                loc[i][2],
                loc[i][3],
                loc[i][4],
                loc[i][5],
                loc[i][6],
                loc[i][7],
            ]) 
    return render(request, 'trigger5/detail-batch.html', response)
