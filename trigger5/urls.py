from django.urls import path

from . import views

app_name = 'trigger5'

urlpatterns = [
    path('', views.index, name='index'),
    path('faskes', views.faskes, name='faskes'),
    path('create-faskes', views.create_faskes, name='create_faskes'),
    path('update-faskes', views.update_faskes, name='update_faskes'),
    path('delete-faskes', views.delete_faskes, name='delete_faskes'),
    path('warehouse', views.warehouse, name='warehouse'),
    path('create-warehouse', views.create_warehouse, name='create_warehouse'),
    path('update-warehouse', views.update_warehouse, name='update_warehouse'),
    path('delete-warehouse', views.delete_warehouse, name='delete_warehouse'),
    path('batch-pengiriman', views.batch, name='batch'),
    path('batch-lokasi', views.batch_lokasi, name='batch_lokasi'),
    path('detail', views.detail_batch, name='detail_batch')
]