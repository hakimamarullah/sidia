from django.urls import path

from . import views

app_name = 'trigger2'

urlpatterns = [
    path('', views.index, name='index'),
    path('permohonan-sumber-daya', views.permohonan_sumber_daya, name='permohonan_sumber_daya'),
    path('create-permohonan-sumber-daya', views.create_permohonan, name='create_permohonan'),
    path('update-permohonan-sumber-daya', views.update_permohonan, name='update_permohonan'),
    path('delete-permohonan-sumber-daya', views.delete_permohonan, name='delete_permohonan'),
    path('riwayat-permohonan-sumber-daya', views.riwayat_permohonan_sumber_daya, name='riwayat_permohonan_sumber_daya'),
    path('create-riwayat', views.create_riwayat, name='create_riwayat'),
    path('proses-riwayat', views.proses_riwayat, name='proses_riwayat'),
    path('proses-riwayat-final', views.proses_riwayat_final, name='proses_riwayat_final'),
    path('item-sumber-daya', views.item_sumber_daya, name='item_sumber_daya'),
    path('create-item', views.create_item, name='create_item'),
    path('update-item', views.update_item, name='update_item'),
    path('delete-item', views.delete_item, name='delete_item'),
]