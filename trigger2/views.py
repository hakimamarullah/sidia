from django.shortcuts import render, redirect
from django.db import connection, IntegrityError
from django.db.utils import InternalError
from django.contrib import messages
from datetime import datetime
from datetime import date
import random


def index(request):
    context = {
        "role" : request.session["role"]
    }

    return render(request, 'trigger2/index.html', context)

def item_sumber_daya(request):
    response = {}
    response['items'] = []
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM ITEM_SUMBER_DAYA")
        fetched = cursor.fetchall()
        for i in range(len(fetched)):
            response['items'].append([
                fetched[i][0],
                fetched[i][1],
                fetched[i][2],
                fetched[i][3],
                fetched[i][4],
                fetched[i][5],
            ]) 
    return render(request, 'trigger2/item-sumber-daya.html', response)

def create_item(request):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                #coded = str(request.POST["kode_item"])[2:5]
                cursor.execute(f"""
                    INSERT INTO ITEM_SUMBER_DAYA (kode, username_supplier, nama, harga_satuan, berat_satuan, kode_tipe_item) VALUES
                    ('{request.POST["kode"]}', '{request.POST["user_supplier"]}', '{request.POST["nama"]}'
                    , {request.POST["harga"]}, {request.POST["berat"]}, '{request.POST["kode_item"]}') """)
                
                # return redirect("")
                messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil")
                return redirect("trigger2:item_sumber_daya")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")

    else:
        response['code'] = []
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM TIPE_ITEM")
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['code'].append(fetched[i][0]) 
    return render(request, 'trigger2/create-item.html', response)


def update_item(request):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    SELECT * FROM ITEM_SUMBER_DAYA WHERE kode='{request.POST["kode"]}'""")
                temp_data = cursor.fetchall()

                if (len(temp_data) > 0):
                    coded = str(request.POST["kode_item"])
                    new_supp = str(temp_data[0][1]) if str(request.POST["user_supplier"]) == "" else str(request.POST["user_supplier"])
                    new_name = str(temp_data[0][2]) if str(request.POST["nama"]) == "" else str(request.POST["nama"])
                    new_harga = str(temp_data[0][3]) if str(request.POST["harga"]) == "" else str(request.POST["harga"])
                    new_berat = str(temp_data[0][4]) if str(request.POST["berat"]) == "" else str(request.POST["berat"])
                    new_coded = str(temp_data[0][5]) if str(request.POST["kode_item"]) == "" else coded


                    cursor.execute(f"""
                        UPDATE ITEM_SUMBER_DAYA SET kode='{request.POST["kode"]}', username_supplier='{new_supp}',nama='{new_name}', 
                        harga_satuan={new_harga}, berat_satuan={new_berat}, kode_tipe_item='{new_coded}'  WHERE kode='{request.POST["kode"]}'""")
                    messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil")
                    return redirect("trigger2:item_sumber_daya")
                else:
                    messages.add_message(request, messages.WARNING, f"“Data tidak ada")
    
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Data salah")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    else:
        response['code'] = []
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM TIPE_ITEM")
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['code'].append(fetched[i][0]) 
    return render(request, 'trigger2/update-item.html', response)


def delete_item(request):
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""DELETE FROM ITEM_SUMBER_DAYA WHERE kode='{request.POST["kode"]}'""")
                messages.add_message(request, messages.SUCCESS, f"Penghapusan berhasil")
                return redirect("trigger2:item_sumber_daya")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger2/delete-item.html')


def permohonan_sumber_daya(request):
    response = {}
    response['psd'] = []
    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT p.no_transaksi_sumber_daya, p.username_petugas_faskes, p.catatan, t.tanggal, t.total_item, t.total_berat
            FROM PERMOHONAN_SUMBER_DAYA_FASKES AS p, TRANSAKSI_SUMBER_DAYA AS t WHERE p.no_transaksi_sumber_daya = t.nomor""")
        fetched = cursor.fetchall()
        for i in range(len(fetched)):
            response['psd'].append([
                fetched[i][0],
                fetched[i][1],
                fetched[i][2],
                fetched[i][3],
                fetched[i][4],
                fetched[i][5],
            ]) 
    return render(request, 'trigger2/permohonan-sumber-daya.html', response)

def create_permohonan(request):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                coded = str(request.POST["kode_item"])[2:5]
                date_today = date.today()

                cursor.execute(f""" SELECT nomor FROM TRANSAKSI_SUMBER_DAYA""")
                length = len(cursor.fetchall()) + 1
            
                cursor.execute(f"""
                    INSERT INTO TRANSAKSI_SUMBER_DAYA (nomor, tanggal, total_berat, total_item) VALUES
                    ('{length}', '{date_today}', {request.POST["berat"]}, {request.POST["jumlah"]}) """)

                if request.POST["catatan"] == "":
                    cursor.execute(f"""
                        INSERT INTO PERMOHONAN_SUMBER_DAYA_FASKES (no_transaksi_sumber_daya, username_petugas_faskes, catatan) VALUES
                        ('{length}', '{request.POST["petugas"]}', NULL) """)

                else:
                    cursor.execute(f"""
                        INSERT INTO PERMOHONAN_SUMBER_DAYA_FASKES (no_transaksi_sumber_daya, username_petugas_faskes, catatan) VALUES
                        ('{length}', '{request.POST["petugas"]}', {request.POST["catatan"]}) """)
                
                # return redirect("")
                messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil")
                return redirect("trigger2:permohonan_sumber_daya")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")

    else:
        response['code'] = []
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM ITEM_SUMBER_DAYA")
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['code'].append([
                    fetched[i][0],
                ]) 
    return render(request, 'trigger2/create-permohonan.html', response)

def delete_permohonan(request):
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""DELETE FROM PERMOHONAN_SUMBER_DAYA_FASKES WHERE no_transaksi_sumber_daya='{request.POST["kode"]}'""")
                cursor.execute(f"""DELETE FROM TRANSAKSI_SUMBER_DAYA WHERE nomor='{request.POST["kode"]}'""")
                messages.add_message(request, messages.SUCCESS, f"Penghapusan berhasil")
                return redirect("trigger2:permohonan_sumber_daya")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger2/delete-permohonan.html')

def update_permohonan(request):
    response = {}
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    SELECT p.no_transaksi_sumber_daya, p.username_petugas_faskes, p.catatan, t.tanggal, t.total_item, t.total_berat
                    FROM PERMOHONAN_SUMBER_DAYA_FASKES AS p, TRANSAKSI_SUMBER_DAYA AS t WHERE p.no_transaksi_sumber_daya = t.nomor 
                    AND p.no_transaksi_sumber_daya = {request.POST["nomor"]}""")
                temp_data = cursor.fetchall()

                if (len(temp_data) > 0):
                    coded = str(request.POST["kode_item"])[2:5]
                    date_today = date.today()
                    new_berat = ""
                    new_jumlah = ""
                    new_catatan = ""


                    if str(request.POST["berat"]) == "":
                        new_berat = str(temp_data[0][5])
                    else:
                        new_berat = str(request.POST["berat"])

                    if str(request.POST["jumlah"]) == "":
                        new_jumlah = str(temp_data[0][4])
                    else:
                        new_jumlah = str(request.POST["jumlah"])

                    if str(request.POST["catatan"]) == "":
                        new_catatan = str(temp_data[0][2])
                    else:
                        new_catatan = str(request.POST["catatan"])
                    

                    cursor.execute(f""" UPDATE TRANSAKSI_SUMBER_DAYA SET tanggal='{date_today}', total_berat={new_berat}, total_item={new_jumlah} WHERE nomor = {request.POST["nomor"]}""")
                    cursor.execute(f""" UPDATE PERMOHONAN_SUMBER_DAYA_FASKES SET catatan='{new_catatan}' WHERE no_transaksi_sumber_daya = {request.POST["nomor"]}""")
                    # cursor.execute(f"""
                    #     INSERT INTO TRANSAKSI_SUMBER_DAYA (nomor, tanggal, total_berat, total_item) VALUES
                    #     ('{length}', '{date_today}', {request.POST["berat"]}, {request.POST["jumlah"]}) """)

                    # if request.POST["catatan"] == "":
                    #     cursor.execute(f"""
                    #         INSERT INTO PERMOHONAN_SUMBER_DAYA_FASKES (no_transaksi_sumber_daya, username_petugas_faskes, catatan) VALUES
                    #         ('{length}', '{request.POST["petugas"]}', NULL) """)

                    # else:
                    #     cursor.execute(f"""
                    #         INSERT INTO PERMOHONAN_SUMBER_DAYA_FASKES (no_transaksi_sumber_daya, username_petugas_faskes, catatan) VALUES
                    #         ('{length}', '{request.POST["petugas"]}', {request.POST["catatan"]}) """)
                    
                # return redirect("")
                messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil")
                return redirect("trigger2:permohonan_sumber_daya")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")

    else:
        response['code'] = []
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM ITEM_SUMBER_DAYA")
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['code'].append([
                    fetched[i][0],
                ]) 
    return render(request, 'trigger2/update-permohonan.html', response)

def delete_permohonan(request):
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""DELETE FROM PERMOHONAN_SUMBER_DAYA_FASKES WHERE no_transaksi_sumber_daya='{request.POST["kode"]}'""")
                messages.add_message(request, messages.SUCCESS, f"Penghapusan berhasil")
                return redirect("trigger2:permohonan_sumber_daya")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger2/delete-permohonan.html')




def riwayat_permohonan_sumber_daya(request):
    response = {}
    # if request.method == "POST":
    #     with connection.cursor() as cursor:
    #         try:
    #             coded = str(request.POST["kode_item"])[2:5]
    #             date_today = date.today()

    #             cursor.execute(f""" SELECT nomor FROM TRANSAKSI_SUMBER_DAYA""")
    #             length = len(cursor.fetchall()) + 1
            
    #             cursor.execute(f"""
    #                 INSERT INTO TRANSAKSI_SUMBER_DAYA (nomor, tanggal, total_berat, total_item) VALUES
    #                 ('{length}', '{date_today}', {request.POST["berat"]}, {request.POST["jumlah"]}) """)

    #             if request.POST["catatan"] == "":
    #                 cursor.execute(f"""
    #                     INSERT INTO PERMOHONAN_SUMBER_DAYA_FASKES (no_transaksi_sumber_daya, username_petugas_faskes, catatan) VALUES
    #                     ('{length}', '{request.POST["petugas"]}', NULL) """)

    #             else:
    #                 cursor.execute(f"""
    #                     INSERT INTO PERMOHONAN_SUMBER_DAYA_FASKES (no_transaksi_sumber_daya, username_petugas_faskes, catatan) VALUES
    #                     ('{length}', '{request.POST["petugas"]}', {request.POST["catatan"]}) """)
                
    #             # return redirect("")
    #             messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil")
    #             return redirect("trigger2:permohonan_sumber_daya")
      
    #         except InternalError:
    #             messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
    #         except:
    #             messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")

    # else:
    response['riwayats'] = []
    with connection.cursor() as cursor:
        cursor.execute(f"""SELECT r.nomor_permohonan, r.kode_status_permohonan, r.username_admin, r.tanggal, s.nama 
            FROM RIWAYAT_STATUS_PERMOHONAN r JOIN STATUS_PERMOHONAN s ON r.kode_status_permohonan = s.kode""")
        fetched = cursor.fetchall()
        for i in range(len(fetched)):
            # temp = ""
            # temp2 =  str(fetched[i][0])[2:5]
            # if temp2 == "REQ":
            #     temp = "Request"
            # elif temp2 == "PRO":
            #     temp = "Process"
            # elif temp2 == "REJ":
            #     temp = "Reject"
            # elif temp2 == "MAS":
            #     temp = "Selesai dengan masalah"
            # elif temp2 == "FIN":
            #     temp = "Selesai"
            

            response['riwayats'].append([
                fetched[i][0],
                fetched[i][1],
                fetched[i][2],
                fetched[i][3],
                fetched[i][4],

            ]) 
    return render(request, 'trigger2/riwayat-status-permohonan.html', response)


def create_riwayat(request):
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                date_today = date.today()

                cursor.execute(f""" SELECT nomor FROM TRANSAKSI_SUMBER_DAYA""")
                length = len(cursor.fetchall()) + 1
            
                cursor.execute(f"""
                    INSERT INTO RIWAYAT_STATUS_PERMOHONAN (kode_status_permohonan, nomor_permohonan , username_admin , tanggal) VALUES
                    ('REQ', '{request.POST["nomor"]}', '{request.POST["admin"]}', '{date_today}') """)

                messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil")
                return redirect("trigger2:permohonan_sumber_daya")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")

    return render(request, 'trigger2/create-riwayat.html')

def proses_riwayat(request):
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""SELECT * FROM RIWAYAT_STATUS_PERMOHONAN WHERE nomor_permohonan = { request.POST["kode"] } AND kode_status_permohonan = 'REQ' """)
                temp_data = cursor.fetchall()

                if (len(temp_data) > 0):
                    date_today = date.today()
                    
                    cursor.execute(f""" UPDATE RIWAYAT_STATUS_PERMOHONAN SET kode_status_permohonan = '{request.POST["acc"]}', tanggal='{date_today}' WHERE nomor_permohonan = { request.POST["kode"] }""")
                   
                messages.add_message(request, messages.SUCCESS, f"Proses berhasil")
                return redirect("trigger2:riwayat_permohonan_sumber_daya")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")

    return render(request, 'trigger2/proses-riwayat.html')

def proses_riwayat_final(request):
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""SELECT * FROM RIWAYAT_STATUS_PERMOHONAN WHERE nomor_permohonan = { request.POST["kode"] } AND kode_status_permohonan = 'PRO' """)
                temp_data = cursor.fetchall()

                if (len(temp_data) > 0):
                    date_today = date.today()
                    
                    cursor.execute(f""" UPDATE RIWAYAT_STATUS_PERMOHONAN SET kode_status_permohonan = '{request.POST["acc"]}', tanggal='{date_today}' WHERE nomor_permohonan = { request.POST["kode"] }""")
                   
                messages.add_message(request, messages.SUCCESS, f"Proses berhasil")
                return redirect("trigger2:riwayat_permohonan_sumber_daya")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")

    return render(request, 'trigger2/proses-riwayat-final.html')