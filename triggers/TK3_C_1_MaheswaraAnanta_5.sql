CREATE OR REPLACE FUNCTION jml_batch_pengiriman()
RETURNS trigger AS
$$
BEGIN
	IF ((SELECT provinsi FROM LOKASI L
    WHERE NEW.id_lokasi_asal = L.id) <> (SELECT provinsi FROM LOKASI LL WHERE NEW.id_lokasi_tujuan = LL.id)
    )
	THEN
	    RAISE EXCEPTION 'Lokasi asal dan tujuan harus memiliki provinsi yang sama';
	END IF;
	RETURN NEW;

END
$$
LANGUAGE plpgsql;

CREATE TRIGGER cek_lokasi_batch_pengiriman()
BEFORE INSERT OR UPDATE ON BATCH_PENGIRIMAN
FOR EACH ROW EXECUTE PROCEDURE jml_batch_pengiriman();