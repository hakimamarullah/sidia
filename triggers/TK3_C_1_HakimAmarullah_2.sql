CREATE OR REPLACE FUNCTION cek_stock_sumber_daya()
RETURNS TRIGGER AS
$$
              DECLARE
-- A. Variable buat nge update berat dan harga kumulatif setiap DAFTAR_ITEM
    	              temp_berat_satuan INTEGER;
    	              temp_harga_satuan INTEGER;
-- B. Variable  buat nge update total berat dan item di TRANSAKSI
    	              berat_total INTEGER;
    	              jumlah_total INTEGER;
-- C. Variable buat ngecek stock di warehouse provinsi
    	              temp_row RECORD;
    	              is_valid BOOLEAN;
    	              id_lokasi_provinsi VARCHAR(5);
    	    	temp_stock_item INTEGER;
	BEGIN
-- A. Kode di bawah buat nge update berat dan harga kumulatif setiap DAFTAR_ITEM
                        -- ngambil tiap daftar item di transaksi yg sama
        	             FOR temp_row IN
        			SELECT no_urut, jumlah_item, kode_item_sumber_daya, no_transaksi_sumber_daya
        			FROM DAFTAR_ITEM
    	                  	WHERE no_transaksi_sumber_daya = NEW.nomor_permohonan 
    	             LOOP
    	    	    	SELECT harga_satuan, berat_satuan 
    	    	    	     INTO temp_harga_satuan, temp_berat_satuan 
    	    	    	FROM ITEM_SUMBER_DAYA
    	    	    	WHERE kode = temp_row.kode_item_sumber_daya;

    	    	    	UPDATE DAFTAR_ITEM di
    	    	    	SET berat_kumulatif = temp_berat_satuan * temp_row.jumlah_item,
     	    	    	     harga_kumulatif = temp_harga_satuan * temp_row.jumlah_item
    	    	    	WHERE di.no_transaksi_sumber_daya = temp_row.no_transaksi_sumber_daya AND di.no_urut = temp_row.no_urut;
    	            END LOOP;

-- B. Kode di bawah buat nge update total berat dan item di TRANSAKSI  
                        -- ambil semua daftar item yg transaksinya sama kek yg dibuat           
    	              SELECT SUM(berat_kumulatif), SUM(jumlah_item) INTO berat_total, jumlah_total 
    	              FROM DAFTAR_ITEM
    	              WHERE no_transaksi_sumber_daya =  NEW.nomor_permohonan;
                            -- update total berat sm total itemnya
    	              UPDATE TRANSAKSI_SUMBER_DAYA
                        SET total_berat = berat_total, total_item = jumlah_total 
                        WHERE nomor = NEW.nomor_permohonan;

-- C. Kode di bawah buat ngecek stock di warehouse provinsi
    	    	is_valid = TRUE;
                    -- ambil lokasi si petugas fakses yg ngajuin
    	              SELECT id_lokasi INTO id_lokasi_provinsi
     	              FROM FASKES
    	              WHERE username_petugas IN
    	              	(SELECT username_petugas_faskes
    	              	FROM PERMOHONAN_SUMBER_DAYA_FASKES
    	              	WHERE no_transaksi_sumber_daya =  NEW.nomor_permohonan);

		FOR temp_row IN
        			SELECT jumlah_item, kode_item_sumber_daya
        			FROM DAFTAR_ITEM
    	                  	WHERE no_transaksi_sumber_daya = NEW.nomor_permohonan 
    	             LOOP
    	    	    	SELECT jumlah INTO temp_stock_item 
    	    	    	FROM STOCK_WAREHOUSE_PROVINSI swp
    	    	    	WHERE swp.id_lokasi_warehouse = id_lokasi_provinsi 
    	    	    	AND swp.kode_item_sumber_daya = temp_row.kode_item_sumber_daya;
    	    	    	IF(temp_stock_item < temp_row.jumlah_item) THEN 
                            is_valid = FALSE;
    	    	    	END IF;
    	            END LOOP;
    	    	IF(is_valid) THEN NEW.kode_status_permohonan = 'REQ';
    	    	ELSE NEW.kode_status_permohonan = 'WAI';
                END IF;
    	            RETURN NEW;
              END;
$$
LANGUAGE plpgsql;

---------------------------------------------
CREATE TRIGGER trigger_cek_stock_sumber_daya
BEFORE INSERT ON RIWAYAT_STATUS_PERMOHONAN
FOR EACH ROW
EXECUTE PROCEDURE cek_stock_sumber_daya()
