CREATE OR REPLACE FUNCTION CHECK_JUMLAH_ITEM() RETURNS trigger AS 
$$
BEGIN
	UPDATE DAFTAR_ITEM SET harga_kumulatif = (SELECT (jumlah_item*harga_satuan) FROM ITEM_SUMBER_DAYA I, DAFTAR_ITEM D
		WHERE I.kode = D.kode_item_sumber_daya AND D.no_transaksi_sumber_daya = NEW.nomor_pesanan),
		berat_kumulatif = (SELECT (jumlah_item*berat_satuan) FROM ITEM_SUMBER_DAYA I, DAFTAR_ITEM D
		WHERE I.kode = D.kode_item_sumber_daya AND D.no_transaksi_sumber_daya = NEW.nomor_pesanan)
		WHERE no_transaksi_sumber_daya = NEW.nomor_pesanan;

	UPDATE TRANSAKSI_SUMBER_DAYA SET total_berat = (SELECT SUM(berat_kumulatif) FROM ITEM_SUMBER_DAYA I, DAFTAR_ITEM D
		WHERE I.kode = D.kode_item_sumber_daya AND D.no_transaksi_sumber_daya = NEW.nomor_pesanan),
		total_item = (SELECT SUM(jumlah_item) FROM ITEM_SUMBER_DAYA I, DAFTAR_ITEM D
		WHERE I.kode = D.kode_item_sumber_daya AND D.no_transaksi_sumber_daya = NEW.nomor_pesanan)
		WHERE nomor = NEW.nomor_pesanan;

	INSERT INTO RIWAYAT_STATUS_PESANAN VALUES('REQ-SUP', NEW.nomor_pesanan, 
	(SELECT I.username_supplier FROM ITEM_SUMBER_DAYA I, DAFTAR_ITEM D
		WHERE I.kode = D.kode_item_sumber_daya AND D.no_transaksi_sumber_daya = NEW.nomor_pesanan), 
	(SELECT CURRENT_DATE));
	
	RAISE notice 'Supplier % akan menyediakan item yang hendak dipesan', I.username_supplier FROM ITEM_SUMBER_DAYA I JOIN DAFTAR_ITEM D
		ON I.kode = D.kode_item_sumber_daya AND D.no_transaksi_sumber_daya = NEW.nomor_pesanan;

	RETURN NEW;
END;
$$ 
LANGUAGE PLPGSQL;

CREATE TRIGGER validate_pesanan
AFTER INSERT ON PESANAN_SUMBER_DAYA
FOR EACH ROW
EXECUTE PROCEDURE CHECK_JUMLAH_ITEM();
