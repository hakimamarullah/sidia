CREATE OR REPLACE FUNCTION CHECK_JUMLAH_BATCH() 
RETURNS trigger AS 
$$
BEGIN
    IF ((SELECT total_berat FROM PERMOHONAN_SUMBER_DAYA_FASKES P, TRANSAKSI_SUMBER_DAYA T 
        WHERE NEW.nomor_transaksi_sumber_daya = T.nomor AND T.nomor = P.no_transaksi_sumber_daya) > 
        (SELECT berat_maksimum FROM KENDARAAN K WHERE NEW.no_kendaraan = K.nomor))
    THEN 
        RAISE EXCEPTION 'The vehicle is unable to load your stuffs';
    END IF;
    IF ( (SELECT SUM(total_berat) FROM TRANSAKSI_SUMBER_DAYA)  = 0 )
    THEN 
        RAISE EXCEPTION 'There is no order currently';
    END IF;
    IF ((SELECT berat_kumulatif FROM MANIFES_ITEM M WHERE NEW.kode = M.kode_batch_pengiriman) = 0 )
    THEN 
        RAISE EXCEPTION 'Manifes Item`s cummulative weight cannot be null';
    END IF;
	RETURN NEW;
END;
$$ 
LANGUAGE PLPGSQL;


CREATE TRIGGER validate_batch
AFTER INSERT ON BATCH_PENGIRIMAN
FOR EACH ROW EXECUTE PROCEDURE CHECK_JUMLAH_BATCH();


-- IF ( (SELECT SUM(total_berat) FROM PERMOHONAN_SUMBER_DAYA_FASKES P, TRANSAKSI_SUMBER_DAYA T 
--         WHERE NEW.nomor_transaksi_sumber_daya = T.nomor AND T.nomor = P.no_transaksi_sumber_daya)  = 0 )