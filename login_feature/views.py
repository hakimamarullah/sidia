from django.shortcuts import render, redirect
from django.db import connection, IntegrityError
from django.db.utils import InternalError
from django.contrib import messages
from datetime import datetime
import random

def login(request):
    # Check apakah sudah logged in, bila iya redirect ke landing
    if request.session.get("pengguna", False):
        return redirect("home:index")

    if request.method == "POST":
        is_found = False # Sudah ketemu?
        tipe_login = None # Pengunjung / Individu / Perusahaan

        with connection.cursor() as cursor:
            # Check apakah ADMIN SATGAS
            tipe_login = "admin_satgas"
            cursor.execute(f"""SELECT * FROM PENGGUNA
                NATURAL JOIN ADMIN_SATGAS
                WHERE username = '{request.POST["username"]}'
                AND password = '{request.POST["password"]}' """)  

            row = cursor.fetchall()
            if (len(row) != 0):
                is_found = True

            # Check apakah PETUGAS_FASKES
            if not is_found:
                tipe_login = "petugas_faskes"
                cursor.execute(f"""SELECT * FROM PENGGUNA
                    NATURAL JOIN PETUGAS_FASKES
                    WHERE username = '{request.POST["username"]}'
                    AND password = '{request.POST["password"]}'""")  

                row = cursor.fetchall()
                if (len(row) != 0):
                    is_found = True
                
            # Check apakah SUPPLIER
            if not is_found:
                tipe_login = "supplier"
                cursor.execute(f"""SELECT * FROM PENGGUNA
                    NATURAL JOIN SUPPLIER
                    WHERE username = '{request.POST["username"]}'
                    AND password = '{request.POST["password"]}'""")  

                row = cursor.fetchall()
                if (len(row) != 0):
                    is_found = True

            # Check apakah DISTRIBUSI
            if not is_found:
                tipe_login = "petugas_distribusi"
                cursor.execute(f"""SELECT * FROM PENGGUNA
                    NATURAL JOIN PETUGAS_DISTRIBUSI
                    WHERE username = '{request.POST["username"]}'
                    AND password = '{request.POST["password"]}'""")  

                row = cursor.fetchall()
                if (len(row) != 0):
                    is_found = True

            # Handler bila ditemukan
            if (len(row) != 0):
                request.session["role"] = tipe_login
                pengguna = row[0][0]
                request.session["pengguna"] = pengguna
                request.session["password"] = row[0][1] # Dapatkan password
                return redirect("home:index")
            
            # Handler bila tidak ditemukan penggunanya
            messages.add_message(request, messages.WARNING, f"Login gagal, check kembali password atau username")
    
    return render(request, "login_feature/logging_inside.html")

def logout(request):
    # Hilangkan semua dari session
    request.session.pop("pengguna", None)
    request.session.pop("role", None)
    return redirect("home:index")

def admin_satgas(request):
    # Check apakah sudah login
    if request.session.get("pengguna", False):
        return redirect("home:index")

    if request.method == "POST":
        # else: # Kalau valid semua, register
        with connection.cursor() as cursor:
            try:
                nama_lst = request.POST["username"].split(" ")
                is_valid = False
                cursor.execute(f"""
                    SELECT * FROM PENGGUNA WHERE username = '{request.POST["username"]}'
                """)

                # Kalau gak ada PENGGUNA dengan email sama maka lanjut bikinin
                if (len(cursor.fetchall()) == 0):
                    cursor.execute(f"""
                        INSERT INTO PENGGUNA (username, password, nama, alamat_kel, alamat_kec, alamat_kabkot, alamat_prov, no_telepon, role) VALUES
                        ('{request.POST["username"]}', '{request.POST["password"]}', '{request.POST["nama"]}', '{request.POST["alamat_kel"]}'
                        , '{request.POST["alamat_kec"]}', '{request.POST["alamat_kabkot"]}', '{request.POST["alamat_prov"]}', '{request.POST["no_telepon"]}'
                        , 'admin_satgas')
                    """)
                
                # return redirect("")
                cursor.execute(f"""INSERT INTO ADMIN_SATGAS (username) VALUES ('{request.POST["username"]}') """)
                messages.add_message(request, messages.SUCCESS, f"Pendaftaran sebagai admin satgas berhasil dilakukan")
                return redirect("login_feature:login")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Username tersebut sudah terdaftar dengan peran admin satgas")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")


    return render(request, "login_feature/admin_satgas.html")

def petugas_faskes(request):
    # Check apakah sudah login
    if request.session.get("pengguna", False):
        return redirect("home:index")

    if request.method == "POST":
        # else: # Kalau valid semua, register
        with connection.cursor() as cursor:
            try:
                nama_lst = request.POST["username"].split(" ")
                is_valid = False
                cursor.execute(f"""
                    SELECT * FROM PENGGUNA WHERE username = '{request.POST["username"]}'
                """)

                # Kalau gak ada PENGGUNA dengan email sama maka lanjut bikinin
                if (len(cursor.fetchall()) == 0):
                    cursor.execute(f"""
                        INSERT INTO PENGGUNA (username, password, nama, alamat_kel, alamat_kec, alamat_kabkot, alamat_prov, no_telepon, role) VALUES
                        ('{request.POST["username"]}', '{request.POST["password"]}', '{request.POST["nama"]}', '{request.POST["alamat_kel"]}'
                        , '{request.POST["alamat_kec"]}', '{request.POST["alamat_kabkot"]}', '{request.POST["alamat_prov"]}', '{request.POST["no_telepon"]}'
                        , 'petugas_faskes')
                    """)
                
                # return redirect("")
                cursor.execute(f"""INSERT INTO PETUGAS_FASKES (username) VALUES ('{request.POST["username"]}') """)
                    
                messages.add_message(request, messages.SUCCESS, f"Pendaftaran sebagai petugas faskes berhasil dilakukan")
                return redirect("login_feature:login")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"Username tersebut sudah terdaftar dengan peran petugas faskes")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")


    return render(request, "login_feature/petugas_faskes.html")

def supplier(request):
    # Check apakah sudah login
    if request.session.get("pengguna", False):
        return redirect("home:index")

    if request.method == "POST":
        # else: # Kalau valid semua, register
        with connection.cursor() as cursor:
            try:
                nama_lst = request.POST["username"].split(" ")
                is_valid = False
                cursor.execute(f"""
                    SELECT * FROM PENGGUNA WHERE username = '{request.POST["username"]}'
                """)

                # Kalau gak ada PENGGUNA dengan email sama maka lanjut bikinin
                if (len(cursor.fetchall()) == 0):
                    cursor.execute(f"""
                        INSERT INTO PENGGUNA (username, password, nama, alamat_kel, alamat_kec, alamat_kabkot, alamat_prov, no_telepon, role) VALUES
                        ('{request.POST["username"]}', '{request.POST["password"]}', '{request.POST["nama"]}', '{request.POST["alamat_kel"]}'
                        , '{request.POST["alamat_kec"]}', '{request.POST["alamat_kabkot"]}', '{request.POST["alamat_prov"]}', '{request.POST["no_telepon"]}'
                        , 'supplier')
                    """)
                
                # return redirect("")
                cursor.execute(f"""INSERT INTO SUPPLIER (username, nama_organisasi) VALUES ('{request.POST["username"]}', '{request.POST["nama_organisasi"]}') """)
                    
                messages.add_message(request, messages.SUCCESS, f"Pendaftaran sebagai supplier berhasil dilakukan")
                return redirect("login_feature:login")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"Username tersebut sudah terdaftar dengan peran supplier")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")


    return render(request, "login_feature/supplier.html")



def petugas_distribusi(request):
    # Check apakah sudah login
    if request.session.get("pengguna", False):
        return redirect("home:index")

    if request.method == "POST":
        # else: # Kalau valid semua, register
        with connection.cursor() as cursor:
            try:
                nama_lst = request.POST["username"].split(" ")
                is_valid = False
                cursor.execute(f"""
                    SELECT * FROM PENGGUNA WHERE username = '{request.POST["username"]}'
                """)

                # Kalau gak ada PENGGUNA dengan email sama maka lanjut bikinin
                if (len(cursor.fetchall()) == 0):
                    cursor.execute(f"""
                        INSERT INTO PENGGUNA (username, password, nama, alamat_kel, alamat_kec, alamat_kabkot, alamat_prov, no_telepon, role) VALUES
                        ('{request.POST["username"]}', '{request.POST["password"]}', '{request.POST["nama"]}', '{request.POST["alamat_kel"]}'
                        , '{request.POST["alamat_kec"]}', '{request.POST["alamat_kabkot"]}', '{request.POST["alamat_prov"]}', '{request.POST["no_telepon"]}'
                        , 'petugas_distribusi')
                    """)
                
                # return redirect("")
                cursor.execute(f"""INSERT INTO PETUGAS_DISTRIBUSI (username, no_sim) VALUES ('{request.POST["username"]}', '{request.POST["no_sim"]}') """)
                
                messages.add_message(request, messages.SUCCESS, f"Pendaftaran sebagai petugas distribusi berhasil dilakukan")
                return redirect("login_feature:login")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"Username tersebut sudah terdaftar dengan peran petugas distribusi")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")


    return render(request, "login_feature/petugas_distribusi.html")

def register(request):
    return render(request, "login_feature/register.html")

# def register_pengunjung(request):
#     # Check apakah sudah login
#     if request.session.get("pengguna", False):
#         return redirect("home:index")

#     if request.method == "POST":
#         # else: # Kalau valid semua, register
#         with connection.cursor() as cursor:
#             try:
#                 nama_lst = request.POST["username"].split(" ")
#                 is_valid = False
#                 roles = request.POST["role"].lower()
#                 cursor.execute(f"""
#                     SELECT * FROM PENGGUNA WHERE username = '{request.POST["username"]}'
#                 """)

#                 # Kalau gak ada PENGGUNA dengan email sama maka lanjut bikinin
#                 if (len(cursor.fetchall()) == 0):
#                     cursor.execute(f"""
#                         INSERT INTO PENGGUNA (username, password, nama, alamat_kel, alamat_kec, alamat_kabkot, alamat_prov, no_telepon, role) VALUES
#                         ('{request.POST["username"]}', '{request.POST["password"]}', '{request.POST["nama"]}', '{request.POST["alamat_kel"]}'
#                         , '{request.POST["alamat_kec"]}', '{request.POST["alamat_kabkot"]}', '{request.POST["alamat_prov"]}', '{request.POST["no_telepon"]}'
#                         , '{request.POST["role"].lower()}')
#                     """)
                
#                 # return redirect("")
#                 if (roles == "admin_satgas"):
#                     cursor.execute(f"""
#                         INSERT INTO ADMIN_SATGAS (username) VALUES ('{request.POST["username"]}') """)
#                     messages.add_message(request, messages.SUCCESS, f"Registrasi berhasil, silahkan login")
#                     return redirect("login_feature:login")
#                 elif (roles == "petugas_faskes"):
#                     cursor.execute(f"""
#                         INSERT INTO PETUGAS_FASKES (username) VALUES ('{request.POST["username"]}') """)
#                     messages.add_message(request, messages.SUCCESS, f"Registrasi berhasil, silahkan login")
#                     return redirect("login_feature:login")
#                 elif (roles == "supplier"):
#                     return redirect("login_feature:supplier")
#                 elif (roles == "petugas_distribusi"):
#                     return redirect("login_feature:petugas_distribusi")
#                 else:
#                     messages.add_message(request, messages.WARNING, f"Role tidak valid, tolong pilih antara admin_satgas, petugas_faskes, supplier, atau petugas_faskes")
      
#             except InternalError:
#                 messages.add_message(request, messages.WARNING, f"{request.POST['username']} sudah terdaftar sebagai Pengunjung/Organizer!")
#             except:
#                 messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")


#     return render(request, "login_feature/registering.html")



