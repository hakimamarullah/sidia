from django.urls import path

from . import views

app_name = 'login_feature'

urlpatterns = [
    path('', views.logout, name='logout'),
    path('login', views.login, name='login'),
    path('register', views.register, name='register'),
    path('supplier', views.supplier, name='supplier'),
    path('petugas-distribusi', views.petugas_distribusi, name='petugas_distribusi'),
    path('admin-satgas', views.admin_satgas, name='admin_satgas'),
    path('petugas-faskes', views.petugas_faskes, name='petugas_faskes'),
]