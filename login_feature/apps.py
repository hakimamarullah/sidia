from django.apps import AppConfig


class LoginFeatureConfig(AppConfig):
    name = 'login_feature'
