from django.urls import path

from . import views

app_name = 'trigger4'

urlpatterns = [
    path('', views.index, name='index'),
    path('lokasi', views.lokasi, name='lokasi'),
    path('form-lokasi', views.form_lokasi, name='form_lokasi'),
    path('update-lokasi', views.update_lokasi, name='update_lokasi'),
    path('batch-pengiriman', views.batch_pengiriman, name='batch_pengiriman'),
    path('create-batch', views.create_batch, name='create_batch'),
    path('detail-batch', views.detail_batch, name='detail_batch'),
    path('kendaraan', views.kendaraan, name='kendaraan'),
    path('create-kendaraan', views.create_kendaraan, name='create_kendaraan'),
    path('update-kendaraan', views.update_kendaraan, name='update_kendaraan'),
    path('delete-kendaraan', views.delete_kendaraan, name='delete_kendaraan'),
]