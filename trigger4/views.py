from django.shortcuts import render, redirect
from django.db import connection, IntegrityError
from django.db.utils import InternalError
from django.contrib import messages
from datetime import datetime
import random

# Create your views here.
def index(request):
    # context = {}
    # try:
    context = {
        "role" : request.session["role"]
    }
    # except:
    #     messages.add_message(request, messages.WARNING, f"Anda belum login")

    return render(request, 'trigger4/index.html', context)


def lokasi(request):
    response = {}
    response['location'] = []
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM LOKASI")
        loc = cursor.fetchall()
        for i in range(len(loc)):
            # cursor.execute("SELECT ID FROM TEMA WHERE ID_event = %s", [Anevent[i][0]])
            # loc_tuple = cursor.fetchall()
            # locations = []
            # for louc in loc_tuple:
            #     locations.append(louc[0])

            response['location'].append([
                loc[i][0],
                loc[i][1],
                loc[i][2],
                loc[i][3],
                loc[i][4],
                loc[i][5]
            ]) 
    return render(request, 'trigger4/lokasi.html', response)

def update_lokasi(request):
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    SELECT * FROM LOKASI WHERE id='{request.POST["id"]}'""")
                idnom = len(cursor.fetchall())

                if (idnom > 0):
                # cursor.execute(f"""
                #     UPDATE LOKASI SET id = '{request.POST["id"]}', provinsi = '{request.POST["provinsi"]}',
                #     kabkot = '{request.POST["kabkot"]}', kecamatan = '{request.POST["kecamatan"]}',
                #     kelurahan = '{request.POST["kelurahan"]}', jalan_no = '{request.POST["jalan"]}',
                #     id_tipe = '{kode}'
                #     WHERE id_event = '{id_event}'
                # """)
                    new_provinsi = ""
                    new_kabkot = ""
                    new_kecamatan = ""
                    new_kelurahan = ""
                    new_jalan = ""

                    cursor.execute(f"""
                            SELECT * FROM LOKASI WHERE id='{request.POST["id"]}'""")
                    temp_data = cursor.fetchall()
                
                    if str(request.POST["provinsi"]) == "":
                        new_provinsi = str(temp_data[0][1])
                    else:
                        new_provinsi = str(request.POST["provinsi"])

                    if str(request.POST["kabkot"]) == "":
                        new_kabkot = str(temp_data[0][2])
                    else:
                        new_kabkot = str(request.POST["kabkot"])

                    if str(request.POST["kecamatan"]) == "":
                        new_kecamatan = str(temp_data[0][3])
                    else:
                        new_kecamatan = str(request.POST["kecamatan"])

                    if str(request.POST["kelurahan"]) == "":
                        new_kelurahan = str(temp_data[0][4])
                    else:
                        new_kelurahan = str(request.POST["kelurahan"])

                    if str(request.POST["jalan"]) == "":
                        new_jalan = str(temp_data[0][5])
                    else:
                        new_jalan = str(request.POST["jalan"])

                    cursor.execute(f"""
                        UPDATE LOKASI SET id='{request.POST["id"]}', provinsi='{new_provinsi}', 
                        kabkot='{new_kabkot}', kecamatan='{new_kecamatan}', kelurahan='{new_kelurahan}'
                        , jalan_no='{new_jalan}' WHERE id='{request.POST["id"]}'""")
                    messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil")
                    return redirect("trigger4:lokasi")
                else:
                    messages.add_message(request, messages.WARNING, f"“Data salah")
    
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Data salah")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger4/update-lokasi.html')


def form_lokasi(request):
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    SELECT * FROM LOKASI""")
                idnom = len(cursor.fetchall()) + 1

                # Kalau gak ada PENGGUNA dengan email sama maka lanjut bikinin
                # if (idnom < 10):
                #     cursor.execute(f"""
                #         INSERT INTO LOKASI (id, provinsi, kabkot, kecamatan, kelurahan, jalan_no) VALUES
                #         ('Loc0{idnom}', '{request.POST["provinsi"]}', '{request.POST["kabkot"]}', '{request.POST["kecamatan"]}'
                #         , '{request.POST["kelurahan"]}', '{request.POST["jalan"]}') """)
                # else:
                cursor.execute(f"""
                    INSERT INTO LOKASI (id, provinsi, kabkot, kecamatan, kelurahan, jalan_no) VALUES
                    ('Loc{str(idnom).zfill(2)}', '{request.POST["provinsi"]}', '{request.POST["kabkot"]}', '{request.POST["kecamatan"]}'
                    , '{request.POST["kelurahan"]}', '{request.POST["jalan"]}') """)
                
                # return redirect("")
                messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil")
                return redirect("trigger4:lokasi")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger4/form_lokasi.html')

def batch_pengiriman(request):
    # response = {}
    # response['batches'] = []
    # with connection.cursor() as cursor:
    #     # cursor.execute(f""" SELECT b.no_transaksi_sumber_daya, f.username_petugas, f.kode_faskes_nasional, 
    #     #     t.total_berat, rsp.kode_status_permohonan , rws.kode_status_batch_pengiriman
    #     #     FROM BATCH_PENGIRIMAN AS b
    #     #     JOIN TRANSAKSI_SUMBER_DAYA AS t ON t.nomor = b.no_transaksi_sumber_daya
    #     #     JOIN PERMOHONAN_SUMBER_DAYA_FASKES AS psdf ON psdf.no_transaksi_sumber_daya = t.nomor
    #     #     JOIN RIWAYAT_STATUS_PERMOHONAN AS rsp ON psdf.no_transaksi_sumber_daya = rsp.nomor_permohonan
    #     #     JOIN RIWAYAT_STATUS_PENGIRIMAN AS rws ON b.kode = rws.kode_batch
    #     #     JOIN LOKASI AS l ON b.id_lokasi_tujuan  = l.id
    #     #     JOIN FASKES AS f ON f.id_lokasi = l.id """)
    #     cursor.execute(f""" SELECT * FROM PERMOHONAN_""")
    #     loc = cursor.fetchall()
    #     for i in range(len(loc)):

    #         response['batches'].append([
    #             loc[i][0],
    #             loc[i][1],
    #             loc[i][2],
    #             loc[i][3],
    #             loc[i][4],
    #             loc[i][5],
    #             loc[i][6]
    #         ]) 
    # return render(request, 'trigger4/batch_pengiriman.html', response)
    response = {}
    response['batches'] = []
    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT p.no_transaksi_sumber_daya, p.username_petugas_faskes, p.catatan, t.tanggal, t.total_item, t.total_berat, rsp.kode_status_permohonan 
            FROM PERMOHONAN_SUMBER_DAYA_FASKES AS p, TRANSAKSI_SUMBER_DAYA AS t, RIWAYAT_STATUS_PERMOHONAN rsp
            WHERE p.no_transaksi_sumber_daya = t.nomor AND p.no_transaksi_sumber_daya = rsp.nomor_permohonan """)
        fetched = cursor.fetchall()

        for i in range(len(fetched)):
            response['batches'].append([
                fetched[i][0],
                fetched[i][1],
                fetched[i][2],
                fetched[i][3],
                fetched[i][4],
                fetched[i][5],
                fetched[i][6],
            ]) 
    return render(request, 'trigger4/batch_pengiriman.html', response)

def detail_batch(request):
    response = {}
    response['batches'] = []
    with connection.cursor() as cursor:
        cursor.execute(f""" SELECT * FROM BATCH_PENGIRIMAN """)
        loc = cursor.fetchall()
        for i in range(len(loc)):

            response['batches'].append([
                loc[i][0],
                loc[i][1],
                loc[i][2],
                loc[i][3],
                loc[i][4],
                loc[i][5],
                loc[i][6],
                loc[i][7],
            ]) 
    return render(request, 'trigger4/detail-batch.html', response)


def create_batch(request):
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:

                cursor.execute(f"""SELECT * FROM TRANSAKSI_SUMBER_DAYA WHERE nomor={request.POST["no_trans"]}""")
                length = cursor.fetchall()
                if len(length) != 0:
                    new_petugas = ""
                    cursor.execute(f"""
                        SELECT * FROM BATCH_PENGIRIMAN""")
                    idnom = len(cursor.fetchall()) + 1
                    #user_acc = request.session.get("pengguna", False)
                    # user_acc = str(request.session["pengguna"])
                    # cursor.execute(f"""SELECT nomor FROM KENDARAAN WHERE nama = '{request.POST["kendaraan"]}'""")
                    # carz = str(cursor.fetchall())

                    # cursor.execute(f"""
                    #     SELECT * FROM PETUGAS_DISTRIBUSI WHERE username='{request.POST["petugas"]}'""")
                    # idnome = len(cursor.fetchall())

                    # request.POST["nama_admin"]
                    
                    if str(request.POST["petugas"]) == "":
                        cursor.execute(f"""
                            INSERT INTO BATCH_PENGIRIMAN (kode, username_satgas, username_petugas_distribusi, tanda_terima, no_transaksi_sumber_daya ,id_lokasi_asal, id_lokasi_tujuan, no_kendaraan) VALUES
                            ('B{str(idnom).zfill(2)}', '{request.POST["nama_admin"]}', NULL, '{request.POST["tterima"]}', {request.POST["no_trans"]}
                            , NULL, NULL, NULL) """)
                    else:
                        cursor.execute(f"""
                            INSERT INTO BATCH_PENGIRIMAN (kode, username_satgas, username_petugas_distribusi, tanda_terima, no_transaksi_sumber_daya ,id_lokasi_asal, id_lokasi_tujuan, no_kendaraan) VALUES
                            ('B{str(idnom).zfill(2)}', '{request.POST["nama_admin"]}', '{request.POST["petugas"]}', '{request.POST["tterima"]}', {request.POST["no_trans"]}
                            , NULL, NULL, NULL) """)

                                

                # cursor.execute(f"""
                #     INSERT INTO BATCH_PENGIRIMAN (kode, username_satgas, username_petugas_distribusi, tanda_terima, Nomor_transaksi_sumber_daya ,id_lokasi_asal, id_lokasi_tujuan, no_kendaraan) VALUES
                #     ('B{str(idnom).zfill(2)}', '{user_acc}', NULL, '{request.POST["tterima"]}', '{request.POST["no_trans"]}'
                #     , NULL, NULL, NULL) """)
              
            #   cursor.execute(f"""
            #         INSERT INTO BATCH_PENGIRIMAN (kode, username_satgas, username_petugas_distribusi, tanda_terima, Nomor_transaksi_sumber_daya ,id_lokasi_asal, id_lokasi_tujuan, nomor_kendaraan) VALUES
            #         ('B{str(idnom).zfill(2)}', '{user_acc}', NULL, '{request.POST["tterima"]}', '{request.POST["no_trans"]}'
            #         , '{request.POST["asal"]}', '{request.POST["tujuan"]}', '{carz}') """)
                messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil")
                return redirect("trigger4:batch_pengiriman")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh atau Petugas Tidak Ditemukan atau Nama Admin tidak terdaftar")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger4/create_batch.html')


def kendaraan(request):
    response = {}
    response['vehicles'] = []
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM KENDARAAN")
        fetched = cursor.fetchall()
        for i in range(len(fetched)):
            response['vehicles'].append([
                fetched[i][0],
                fetched[i][1],
                fetched[i][2],
                fetched[i][3],
            ]) 
    return render(request, 'trigger4/kendaraan.html', response)

def create_kendaraan(request):
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                
                cursor.execute(f"""
                    INSERT INTO KENDARAAN (nomor, nama, jenis_kendaraan, berat_maksimum) VALUES
                    ('{request.POST["nomor_kendaraan"]}', '{request.POST["nama"]}', '{request.POST["jenis"]}'
                    , '{request.POST["berat"]}') """)
                
                # return redirect("")
                messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil")
                return redirect("trigger4:kendaraan")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger4/create-kendaraan.html')

def update_kendaraan(request):
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    SELECT * FROM KENDARAAN WHERE nomor='{request.POST["nomor_kendaraan"]}'""")
                idnom = len(cursor.fetchall())

                if (idnom > 0):
                    new_name = ""
                    new_jenis = ""
                    new_berat_maks = ""

                    cursor.execute(f"""
                            SELECT * FROM KENDARAAN WHERE nomor='{request.POST["nomor_kendaraan"]}'""")
                    temp_data = cursor.fetchall()
                
                    if str(request.POST["nama"]) == "":
                        new_name = str(temp_data[0][1])
                    else:
                        new_name = str(request.POST["nama"])

                    if str(request.POST["jenis"]) == "":
                        new_jenis = str(temp_data[0][2])
                    else:
                        new_jenis = str(request.POST["jenis"])

                    if str(request.POST["berat"]) == "":
                        new_berat_maks = str(temp_data[0][3])
                    else:
                        new_berat_maks = str(request.POST["berat"])


                    cursor.execute(f"""
                        UPDATE KENDARAAN SET nomor='{request.POST["nomor_kendaraan"]}', nama='{new_name}', 
                        jenis_kendaraan='{new_jenis}', berat_maksimum='{new_berat_maks}' WHERE nomor='{request.POST["nomor_kendaraan"]}'""")
                    messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil")
                    return redirect("trigger4:kendaraan")
                else:
                    messages.add_message(request, messages.WARNING, f"“Data tidak ada")
    
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Data salah")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger4/update-kendaraan.html')

def delete_kendaraan(request):
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                
                # cursor.execute(f"""
                #     INSERT INTO KENDARAAN (nomor, nama, jenis_kendaraan, berat_maksimum) VALUES
                #     ('{request.POST["nomor_kendaraan"]}', '{request.POST["nama"]}', '{request.POST["jenis"]}'
                #     , '{request.POST["berat"]}') """)

                cursor.execute(f"""DELETE FROM KENDARAAN WHERE nomor='{request.POST["nomor_kendaraan"]}'""")
                
                # return redirect("")
                messages.add_message(request, messages.SUCCESS, f"Penghapusan berhasil")
                return redirect("trigger4:kendaraan")
      
            except InternalError:
                messages.add_message(request, messages.WARNING, f"“Database sudah penuh")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")
    return render(request, 'trigger4/delete-kendaraan.html')